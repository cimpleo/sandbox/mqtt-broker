#!/bin/bash

touch /password_file

if ( [ -z "${ROOT_USERNAME}" ] || [ -z "${ROOT_PASSWORD}" ] ); then
  echo "ROOT_USERNAME or ROOT_PASSWORD not defined"
  exit 1
fi

mosquitto_passwd -b /password_file $ROOT_USERNAME $ROOT_PASSWORD

if ( [ -z "${PSK_IDENTITY}" ] || [ -z "${PSK_KEY}" ] ); then
  echo "PSK_IDENTITY or PSK_KEY not defined"
  exit 1
fi

echo "$PSK_IDENTITY:$PSK_KEY" > /keys.psk

mosquitto -c /mosquitto/mosquitto.conf -v
