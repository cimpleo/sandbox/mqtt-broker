build:
	docker-compose build --no-cache

push:
	docker-compose push

deploy: build push

up:
	docker-compose up -d
shell:
	docker-compose exec mqtt.broker bash
log:
	docker-compose logs -f mqtt.broker
