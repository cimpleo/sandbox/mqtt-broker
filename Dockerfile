FROM ubuntu:18.04

RUN apt-get -y update && \
    apt-get -y install mosquitto mosquitto-clients && \
    apt-get -y install openssl

COPY ./mosquitto.conf /mosquitto/mosquitto.conf
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh


ENTRYPOINT /entrypoint.sh
